<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200903090302 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE years_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE about_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE action_events_id_seq CASCADE');
        $this->addSql('DROP TABLE metrag_public_sub_types');
        $this->addSql('DROP TABLE dbdump');
        $this->addSql('DROP TABLE dbdump_1');
        $this->addSql('DROP TABLE action_events');
        $this->addSql('DROP TABLE years');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE years_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE about_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE action_events_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE years (id INT NOT NULL, year_from VARCHAR(255) DEFAULT NULL, year_to VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE action_events (id BIGINT NOT NULL, batch_id CHAR(36) NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, actionable_type VARCHAR(255) NOT NULL, actionable_id INT NOT NULL, target_type VARCHAR(255) NOT NULL, target_id INT NOT NULL, model_type VARCHAR(255) NOT NULL, model_id INT DEFAULT NULL, fields TEXT NOT NULL, status VARCHAR(25) DEFAULT \'running\' NOT NULL, exception TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL)');
        $this->addSql('CREATE TABLE dbdump_1 (c1 TEXT DEFAULT NULL)');
        $this->addSql('CREATE TABLE dbdump (c1 TEXT DEFAULT NULL)');
        $this->addSql('CREATE TABLE metrag_public_sub_types (c1 TEXT DEFAULT NULL, c2 TEXT DEFAULT NULL, c3 TEXT DEFAULT NULL, c4 NUMERIC(10, 0) DEFAULT NULL, c5 TEXT DEFAULT NULL)');
    }
}
