<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200826110530 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE realties ADD meta_description TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE realties ADD meta_title TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE realties ADD meta_keywords TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE realties ADD title TEXT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE realties DROP meta_description');
        $this->addSql('ALTER TABLE realties DROP meta_title');
        $this->addSql('ALTER TABLE realties DROP meta_keywords');
        $this->addSql('ALTER TABLE realties DROP title');
    }
}
