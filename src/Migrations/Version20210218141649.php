<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210218141649 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE agent_reviews ADD meta_description TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE agent_reviews ADD meta_keywords TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE agents ADD meta_description TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE agents ADD meta_keywords TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE lived_complex_static ADD meta_description TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE lived_complex_static ADD meta_keywords TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE newses ADD meta_description TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE newses ADD meta_keywords TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE seo_page ALTER header_title DROP NOT NULL');
        $this->addSql('ALTER TABLE static_about ADD meta_description TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE static_about ADD meta_keywords TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE static_services_top ALTER text TYPE TEXT');
        $this->addSql('ALTER TABLE static_services_top ALTER text DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE agents DROP meta_description');
        $this->addSql('ALTER TABLE agents DROP meta_keywords');
        $this->addSql('ALTER TABLE lived_complex_static DROP meta_description');
        $this->addSql('ALTER TABLE lived_complex_static DROP meta_keywords');
        $this->addSql('ALTER TABLE newses DROP meta_description');
        $this->addSql('ALTER TABLE newses DROP meta_keywords');
        $this->addSql('ALTER TABLE agent_reviews DROP meta_description');
        $this->addSql('ALTER TABLE agent_reviews DROP meta_keywords');
        $this->addSql('ALTER TABLE seo_page ALTER header_title SET NOT NULL');
        $this->addSql('ALTER TABLE static_services_top ALTER text TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE static_services_top ALTER text DROP DEFAULT');
        $this->addSql('ALTER TABLE static_about DROP meta_description');
        $this->addSql('ALTER TABLE static_about DROP meta_keywords');
    }
}
