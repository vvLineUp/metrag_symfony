<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200901103309 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE seo_page_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE seo_page (id INT NOT NULL, meta_description TEXT DEFAULT NULL, meta_title TEXT DEFAULT NULL, meta_keywords TEXT DEFAULT NULL, title TEXT DEFAULT NULL, description TEXT NOT NULL, url TEXT NOT NULL, url_alias TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E8DCA6F1F47645AE ON seo_page (url)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E8DCA6F124C804E3 ON seo_page (url_alias)');
        $this->addSql('ALTER TABLE realties ADD slug TEXT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE seo_page_id_seq CASCADE');
        $this->addSql('DROP TABLE seo_page');
        $this->addSql('ALTER TABLE realties DROP slug');
    }
}
