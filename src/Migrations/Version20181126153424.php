<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181126153424 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
       // $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

       // $this->addSql('DROP SEQUENCE request_on_view_id_seq CASCADE');
       // $this->addSql('DROP SEQUENCE agent_id_seq CASCADE');
       // $this->addSql('DROP SEQUENCE about_id_seq CASCADE');
       // $this->addSql('DROP SEQUENCE meta_id_seq CASCADE');
       // $this->addSql('DROP SEQUENCE meta_info_id_seq CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        //$this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

       // $this->addSql('CREATE SCHEMA public');
       // $this->addSql('CREATE SEQUENCE request_on_view_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
       // $this->addSql('CREATE SEQUENCE agent_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
       // $this->addSql('CREATE SEQUENCE about_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
       // $this->addSql('CREATE SEQUENCE meta_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
       // $this->addSql('CREATE SEQUENCE meta_info_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
    }
}
