<?php

// src/Twig/AppExtension.php
namespace App\Twig;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Route;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    private $router;

    public function __construct(UrlGeneratorInterface $router)
    {
        $this->router = $router;

    }
    public function getFilters(): array
    {
        return [
            new TwigFilter('checked', array($this, 'checkedFilter'))
        ];
    }

    /**
     * Checked filter in view
     *
     * @param $var1
     * @param $GET
     * @param $key
     * @param $functionName
     * @param $isArray false
     * @return string
     */

    public function checkedFilter($var1, $GET, $key, $functionName, bool $isArray = false): string
    {
        //key is not set in get request
        if(!isset($GET->$key)) {
            return '';
        }

        $expression = false;

        if(!$isArray) {
            $expression = $var1 === $functionName($GET->$key);
        } else {
            $expression = is_array($GET->$key) && in_array($var1, $GET->$key);
        }

        return $expression ? 'checked' : '';
    }

}