<?php
// src/Command/ImportRealtyCommand.php
namespace App\Command;

use App\Metrag\AppBundle\Entity\News;
use App\Metrag\AppBundle\Entity\Agent;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImgCleanerCommand extends ContainerAwareCommand
{

    public function __construct(?string $name = null)
    {
        parent::__construct($name);

    }

    protected function configure()
    {
        $this
            ->setName('app:clean-img')
            ->setDescription('Clean all old images')
            ->setHelp('This command clean images witch not in database.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->cleanNewsImg();
    }

    private function cleanNewsImg()
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $newses = $em->getRepository('AppBundle:News')->findAll();
        $agents = $em->getRepository('AppBundle:Agent')->findAll();

        $this->cleanImages($newses, News::getUploadDir());
        $this->cleanImages($agents, Agent::getUploadDir());

    }

    private function cleanImages(array $arrayFromDB, string $uploadDirForEntity): void
    {
        $basePath = $this->getContainer()->get('kernel')->getRootDir() . '/../public/';
        $imgDir = getenv('UPLOAD_DIR') . '/' . $uploadDirForEntity . '/';
        $fileImages = scandir($basePath . $imgDir);
        unset($fileImages[0], $fileImages[1]);

        foreach($fileImages as $fileImage) {
            if(!$this->isImgInDB($arrayFromDB, $imgDir. $fileImage)) {
                unlink($basePath . $imgDir. $fileImage);
            }
        }
    }

    private function isImgInDB(array $newses, string $img): bool
    {
        foreach($newses as $news) {
            if($news->getImg() === $img) {
                return true;
            }
        }

        return false;
    }
}
