<?php

namespace App\Command;

use App\Metrag\AppBundle\Entity\LivedComplexStatic;
use App\Metrag\AppBundle\Entity\Realty;
use App\Metrag\AppBundle\Entity\Template;
use App\Metrag\AppBundle\Repository\RealtyRepository;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UpdateTemplateRealtyCommand extends Command
{

    private $realtyRepository;
    /**
     * @var Registry $docktrine
     */
    private $docktrine;

    public function __construct(?string $name = null, RealtyRepository $realtyRepository)
    {
        $this->realtyRepository = $realtyRepository;
        parent::__construct($name);
    }
    protected function configure()
    {
        $this
            ->setName('app:set-template')
            ->setDescription('Set template in realties')
            ->setHelp('This command cinsert template for realties witch not in database.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $this->realtyRepository->setTamplateForRealtyTitle();
        $this->realtyRepository->setTamplateForRealtyDescription();

        $io->success('File successfully generated');
        return 0;
    }
}
