<?php

namespace App\Command;

use App\Metrag\ApiBundle\Services\RealtyService;
use App\Metrag\XmlBundle\Services\XmlFeedGeneratorService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GenerateXmlFeedCommand extends Command
{
    /**
     * @var XmlFeedGeneratorService
     */
    private $generateFeedService;
    /**
     * @var RealtyService
     */
    private $realtyService;

    public function __construct(string $name = null, XmlFeedGeneratorService $feedGeneratorService, RealtyService $realtyService)
    {
        $this->generateFeedService = $feedGeneratorService;
        $this->realtyService = $realtyService;
        parent::__construct($name);
    }

    protected static $defaultName = 'app:generate-xml-feed';

    protected function configure()
    {
        $this
            ->setDescription('Create feed.xml in public folder')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $realties =  $this->realtyService->getRealtyOnFilter([])->execute();
        if($this->generateFeedService->generateFile($realties)){
            $io->success('File successfully generated');
        }
        else{
            $io->error('Something went wrong');
        }

        return 0;
    }
}
