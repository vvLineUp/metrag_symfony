<?php

namespace App\Metrag\AppBundle\Repository;

use App\Metrag\AppBundle\Entity\LivedComplex;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LivedComplex|null find($id, $lockMode = null, $lockVersion = null)
 * @method LivedComplex|null findOneBy(array $criteria, array $orderBy = null)
 * @method LivedComplex[]    findAll()
 * @method LivedComplex[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LivedComplexRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LivedComplex::class);
    }

    public function count($filter)
    {
        $qb = $this->createQueryBuilder('r');
        return $qb
            ->select('count(r.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }
    public function removeWhereIdsNotIn(array $ids): bool
    {
        return (bool)$this->createQueryBuilder('r')
            ->delete()
            ->where('r.id NOT IN(' . implode(',', $ids) . ')')
            ->getQuery()
            ->execute();
    }
}
