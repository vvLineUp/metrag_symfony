<?php

namespace App\Metrag\AppBundle\Repository;

use App\Metrag\AppBundle\Entity\DealType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DealType|null find($id, $lockMode = null, $lockVersion = null)
 * @method DealType|null findOneBy(array $criteria, array $orderBy = null)
 * @method DealType[]    findAll()
 * @method DealType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DealTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DealType::class);
    }

}
