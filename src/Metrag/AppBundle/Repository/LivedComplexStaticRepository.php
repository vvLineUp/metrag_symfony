<?php

namespace App\Metrag\AppBundle\Repository;

use App\Metrag\AppBundle\Entity\LivedComplexStatic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LivedComplexStatic|null find($id, $lockMode = null, $lockVersion = null)
 * @method LivedComplexStatic|null findOneBy(array $criteria, array $orderBy = null)
 * @method LivedComplexStatic[]    findAll()
 * @method LivedComplexStatic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LivedComplexStaticRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LivedComplexStatic::class);
    }

    public function count($filter)
    {
        $qb = $this->createQueryBuilder('r');
        return $qb
            ->select('count(r.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }
}
