<?php

namespace App\Metrag\AppBundle\Repository;

use App\Metrag\AppBundle\Entity\SubType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SubType|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubType|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubType[]    findAll()
 * @method SubType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SubType::class);
    }

}
