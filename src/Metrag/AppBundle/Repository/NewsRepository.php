<?php

namespace App\Metrag\AppBundle\Repository;

use App\Metrag\AppBundle\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository
{

    const DEFAULT_LIMIT = 4;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, News::class);
    }

    public function getOnDays(int $days = null, int $limit = null)
    {

        $query = $this->getQueryOnDays($days);

        return $query
            ->setMaxResults($limit ?: self::DEFAULT_LIMIT)
            ->getQuery()
            ->execute();
    }

    public function getQueryOnDays(int $days = null): QueryBuilder
    {
        $query = $this->createQueryBuilder('r');

        if ($days) {
            $datetime = new \DateTime();
            $datetime->modify('-' . $days . '  days');
            $query = $query
                ->where('r.datetime > :datetime')
                ->setParameter('datetime', $datetime);
        }

        return $query
            ->orderBy('r.datetime', 'DESC');
    }

}
