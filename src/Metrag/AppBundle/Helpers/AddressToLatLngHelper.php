<?php

namespace App\Metrag\AppBundle\Helpers;

class AddressToLatLngHelper
{
    public static function convert(string $address): ?array
    {
        $address = str_replace(' ', '+', $address);
        $location = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . 'Харьков,' . $address . '&key=' . getenv('GOOGLE_MAP_KEY')));

        echo 'https://maps.googleapis.com/maps/api/geocode/json?address=' . 'Харьков,' . $address . '&key=' . getenv('GOOGLE_MAP_KEY');
        echo "\n\r";

        //offset in decimal. example: 0.014
        $offset1 = mt_rand(11, 15)/10000;
        $offset2 = mt_rand(11, 15)/10000;

        $lat = $location->results[0]->geometry->location->lat;
        $lng = $location->results[0]->geometry->location->lng;

        return [
            'lat' => $lat ? $lat + $offset1 : 0,
            'lng' => $lng ? $lng + $offset2 : 0
        ];
    }
}