<?php

namespace App\Metrag\AppBundle\Helpers;

class PaginationHelper
{
    public static function getOffset(int $page, int $limit): int
    {
        return $page - 1 ? ($page - 1) * $limit: 0;
    }

    public static function getCounPages(int $totalItems, int $limit): int
    {
        return $totalItems ? (int)ceil($totalItems / $limit) : $limit;
    }

}