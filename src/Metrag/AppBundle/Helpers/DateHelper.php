<?php

namespace App\Metrag\AppBundle\Helpers;

class DateHelper
{
    public static function toShowing(\DateTime $dateTime): string
    {
        $date = '';
        if(self::isToday($dateTime)) {
            $date = $dateTime->format('Сегодня в H:i');
        } else {
            $date = $dateTime->format('d.m.Y H:i');
        }
        return $date;
    }

    private static function isToday(\DateTime $dateTime): bool
    {
        $now = new \DateTime();
        return $dateTime->format('d.m.Y') === $now->format('d.m.Y');
    }
    public static function getDateToMail()
    {
        $date = new \DateTime('now',  new \DateTimeZone('Europe/Zaporozhye'));
        return $date->format('Y-m-d H:i:s');
    }
}