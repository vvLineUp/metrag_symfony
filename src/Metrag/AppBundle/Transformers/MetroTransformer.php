<?php

namespace App\Metrag\AppBundle\Transformers;

use App\Metrag\AppBundle\Entity\Metro;

class MetroTransformer
{
    public function transform(array $metroList): array
    {
        $response = [];
        /** @var Metro $metro */
        foreach ($metroList as $metro) {
            $district = $metro->getDistrict();
            $response[] = [
                'id' => $metro->getId(),
                'name' => $metro->getName(),
                'district_id' => $district ? $district->getId() : 0,
            ];
        }

        return $response;
    }
}