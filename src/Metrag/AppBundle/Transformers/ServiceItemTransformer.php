<?php

namespace App\Metrag\AppBundle\Transformers;

use App\Metrag\AppBundle\Entity\ServiceItem;

class ServiceItemTransformer
{
    public function transform(array $serviceItems): array
    {
        $services = [];
        /** @var $serviceItem ServiceItem */
        foreach($serviceItems as $serviceItem) {

            $services[$serviceItem->getServiceId()->getId()][] = $serviceItem;

        }

        return $services;
    }
}