<?php

// src/Metrag/AppBundle/SitemapController.php

namespace App\Metrag\AppBundle\Controller;

use App\Metrag\AppBundle\Services\LivedComplexService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SitemapController extends Controller
{
    /**
     * @param Request $request
     * @param LivedComplexService $livedComplexService
     * @return Response
     */
    public function sitemapAction(Request $request, LivedComplexService $livedComplexService)
    {
        // Define an array of urls
        $urls = [];
        // Store the hostname of our website
        $hostname = $request->getSchemeAndHttpHost();
       
        // Static urls
        $urls[] = ['loc' => $this->get('router')->generate('home'), 'priority' => '1.0'];
        $urls[] = ['loc' => $this->get('router')->generate('static_page_about'), 'priority' => '0.3'];
        $urls[] = ['loc' => $this->get('router')->generate('static_page_owner'), 'priority' => '0.3'];
        $urls[] = ['loc' => $this->get('router')->generate('static_page_contacts'), 'priority' => '0.3'];
        $urls[] = ['loc' => $this->get('router')->generate('static_page_services'), 'priority' => '0.3'];
        $urls[] = ['loc' => $this->get('router')->generate('lived_complex'), 'priority' => '0.5'];

        //SEO page urls
        $seoPages = $this->getDoctrine()
                            ->getRepository('AppBundle:SeoPage')
                            ->findAll();
        foreach ($seoPages as $seoPage) {
            $urls[] = ['loc' => $this->get('router')->generate('seo_page', ['url_alias' => $seoPage->getUrlAlias()]), 'priority' => '0.8'];
        }

        // Lived complex urls
        $livedComplexes = $livedComplexService->getComplexes();
        foreach ($livedComplexes as $livedComplex) {
            $urls[] = ['loc' => $this->get('router')->generate('lived_complex_show', ['id' => $livedComplex->getId()]), 'priority' => '0.5'];
        }
        
        //Realty urls
        $realty = $this->getDoctrine()
                            ->getRepository('AppBundle:Realty')
                            ->findAll();
        foreach ($realty as $value) {
            $urls[] = ['loc' => $this->get('router')->generate('realty_show', ['id' => $value->getId()]), 'priority' => '0.5'];
        }

        $response = new Response(
            $this->renderView('@App/default/sitemap.xml.twig', [
                'urls' => $urls,
                'hostname' => $hostname
            ])
        );

        $response->headers->set('Content-Type', 'application/xml');

        return $response;
    }

    /**
     * @param Request $request
     * @param LivedComplexService $livedComplexService
     * @return Response
     */
    public function sitemapImageAction(Request $request, LivedComplexService $livedComplexService)
    {
        // Define an array of urls
        $urls = [];
        // Store the hostname of our website
        $hostname = $request->getSchemeAndHttpHost();

        // Lived complex img urls
        $livedComplexes = $livedComplexService->getComplexes();
        foreach ($livedComplexes as $livedComplex) {
            $urls[] = ['loc' =>  $hostname.'/'.$livedComplex->getImg(), 'priority' => '0.5'];
        }

        //Realty img urls
        $realty = $this->getDoctrine()
            ->getRepository('AppBundle:Realty')
            ->findAll();
        foreach ($realty as $value) {
            foreach ($value->getImages() as $image)
            $urls[] = ['loc' => $image, 'priority' => '0.5'];
        }

        $response = new Response(
            $this->renderView('@App/default/sitemap-image.xml.twig', [
                'urls' => $urls,
                'hostname' => $hostname
            ])
        );

        $response->headers->set('Content-Type', 'application/xml');

        return $response;
    }

}