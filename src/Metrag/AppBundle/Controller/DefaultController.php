<?php

// src/Metrag/AppBundle/DefaultController.php

namespace App\Metrag\AppBundle\Controller;

use App\Metrag\ApiBundle\Services\RealtyService;
use App\Metrag\ApiBundle\Transformers\RealtyTransformer;
use App\Metrag\ApiBundle\Transformers\ReviewTransformer;
use App\Metrag\AppBundle\Entity\Realty;
use App\Metrag\AppBundle\Entity\SeoPage;
use App\Metrag\AppBundle\Entity\Status;
use App\Metrag\AppBundle\Entity\Type;
use App\Metrag\AppBundle\Transformers\ServiceItemTransformer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller
{
    public function indexAction(RealtyService $realtyService)
    {
        $counterResidential = $this->getDoctrine()
            ->getRepository('AppBundle:Realty')
            ->countByColumns([Type::TYPES['flat']]);

        $counterCommercial = $this->getDoctrine()
            ->getRepository('AppBundle:Realty')
            ->countByColumns([Type::TYPES['commercial']]);

        $realties = $realtyService->getOnStatus([Status::STATUSES['premium'], Status::STATUSES['top']]);

        $about = $this
            ->getDoctrine()
            ->getRepository('AppBundle:About')
            ->findOneBy([]);

        $reviews = $this
            ->getDoctrine()
            ->getRepository('AppBundle:AgentReview')
            ->findBy(['is_showing' => true], null, 2, 0);

        $firstPremium = $this->shiftFirstPremiumRealty($realties);

        return $this->render('@App/default/index.html.twig', [
            'premium' => $firstPremium ? (new RealtyTransformer())->transform([$firstPremium])[0] : [],
            'realties' => (new RealtyTransformer)->transform($realties),
            'counters' => [
                'residential' => $counterResidential,
                'commercial' => $counterCommercial
            ],
            'about' => $about,
            'reviews' => (new ReviewTransformer())->transform($reviews)
        ]);
    }

    public function ownerAction()
    {
        $dealTypes = $this
            ->getDoctrine()
            ->getRepository('AppBundle:DealType')
            ->findAll();

        $types = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Type')
            ->findAll();

        $districts = $this
            ->getDoctrine()
            ->getRepository('AppBundle:District')
            ->findAll();

        return $this->render('@App/default/owner.html.twig', [
            'deal_types' => $dealTypes,
            'types' => $types,
            'districts' => $districts
        ]);
    }

    public function contactsAction()
    {
        return $this->render('@App/default/contacts.html.twig', [
        ]);
    }

    public function aboutAction()
    {
        $about = $this
            ->getDoctrine()
            ->getRepository('AppBundle:About')
            ->findOneBy([]);

        $growths = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Growth')
            ->findAll();

        $phones = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Phone')
            ->findAll();

        $addresses = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Address')
            ->findAll();

        $email = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Meta')
            ->find(1);

        return $this->render('@App/default/about.html.twig', [
            'about' => $about,
            'growths' => $growths,
            'phones' => $phones,
            'addresses' => $addresses,
            'email' => $email->getText(),
            'meta_description' => $about->getMetaDescription(),
            'meta_keywords' => $about->getMetaKeywords(),
        ]);
    }

    public function servicesAction()
    {
        $servicesTop = $this
            ->getDoctrine()
            ->getRepository('AppBundle:ServiceTop')
            ->findAll();

        $services = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Service')
            ->findAll();

        $serviceItems = $this
            ->getDoctrine()
            ->getRepository('AppBundle:ServiceItem')
            ->findAll([], ['service_id' => 'DESC', 'position_id' => 'DESC']);

        return $this->render('@App/default/services.html.twig', [
            'servicesTop' => $servicesTop,
            'services' => $services,
            'serviceItems' => (new ServiceItemTransformer())->transform($serviceItems),
        ]);
    }

    /**
     * Return Page by url
     * @param $url_alias String
     *
     */
    public function SeoPageAction($url_alias, Request $request)
    {
        $seo_page = $this
            ->getDoctrine()
            ->getRepository('AppBundle:SeoPage')
            ->findOneBy(['url_alias' => $url_alias]);

        $request->getSession()->set('url_alias', $url_alias);


        if ($seo_page == null) {
            throw new NotFoundHttpException();
        } else {
            return $this->render('@App/default/seo_page.html.twig', [
                    'header_title' => $seo_page->getHeaderTitle(),
                    'description' => $seo_page->getDescription(),
                    'current_page' => $url_alias,
                    'title' => $seo_page->getTitle(),
                    'meta_title' => $seo_page->getMetaTitle(),
                    'meta_description' => $seo_page->getMetaDescription(),
                    'meta_keywords' => $seo_page->getMetaKeywords(),
            ]);
        }
    }

    public function getSeoRealtiesAction(Request $request, RealtyService $realtyService)
    {
        $url = $request->getSession()->get('url_alias');
        $realties = $realtyService->getRealties($request, $url);
        return new JsonResponse($realties);
    }


    private function shiftFirstPremiumRealty(array &$realties): ?Realty
    {
        $i = 0;
        foreach($realties as $realty) {
            if($this->isActiveAndPremium($realty)) {
                unset($realties[$i]);
                return $realty;
            }
            ++$i;
        }
        $premiumRealty = null;
        if(!empty($realties[0])) {
            $premiumRealty = $realties[0];
            unset($realties[0]);
        }


        return $premiumRealty;
    }

    private function isActiveAndPremium($realty): bool
    {
        $status = $realty->getStatus();
        return $realty->getIsActive() && $status && $status->getId() === Status::STATUSES['premium'];
    }

}
