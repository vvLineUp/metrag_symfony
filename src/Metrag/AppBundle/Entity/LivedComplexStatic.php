<?php

namespace App\Metrag\AppBundle\Entity;

use App\Metrag\AppBundle\Services\ImgService;
use Doctrine\ORM\Mapping as ORM;

/**
 * LivedComplex
 *
 * @ORM\Table(name="lived_complex_static")
 * @ORM\Entity(repositoryClass="App\Metrag\AppBundle\Repository\LivedComplexStaticRepository")
 */
class LivedComplexStatic
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="img", type="text", nullable=true)
     */
    private $img;

    /**
     * @ORM\ManyToOne(targetEntity="App\Metrag\AppBundle\Entity\Metro", inversedBy="realties")
     * @ORM\JoinColumn(name="metro_id", referencedColumnName="id")
     */
    private $metro;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text", nullable=true)
     */
    private $meta_description;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="text", nullable=true)
     */
    private $meta_keywords;

    public function __toString(): string
    {
        return $this->getName();
    }

    public function setId(int $id)
    {
        return $this->id = $id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img ?? 'img/building.png';
    }

    /**
     * @param string $img
     * @return LivedComplex
     *
     * TODO rewrite to automatically upload file. It's costal, need because in easyadmin can not use path to saving image. vendor libraries do not works.
     */
    public function setImg(string $img = null): self
    {

        if($img) {
            $imgName = ImgService::upload(self::getUploadDir(), $img);
            $this->img = $imgName;
        }

        return $this;
    }
    public function getMetro(): ?Metro
    {
        return $this->metro;
    }

    public function setMetro(?Metro $metro): self
    {
        $this->metro = $metro;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    private static function getUploadDir()
    {
        return 'lived_complex';
    }

    public function getMetaDescription(): ?string
    {
        return $this->meta_description;
    }

    public function setMetaDescription(?string $meta_description): self
    {
        $this->meta_description = $meta_description;

        return $this;
    }

    public function getMetaKeywords(): ?string
    {
        return $this->meta_keywords;
    }

    public function setMetaKeywords(?string $meta_keywords): self
    {
        $this->meta_keywords = $meta_keywords;

        return $this;
    }
}