<?php

namespace App\Metrag\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SubType. list of all subtypes of realty
 *
 * @ORM\Table(name="sub_types")
 * @ORM\Entity
 */
class SubType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text")
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Metrag\AppBundle\Entity\Type", inversedBy="sub_types")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id" )
     */
    private $type;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }
}