<?php

namespace App\Metrag\AppBundle\Entity;

use App\Metrag\AppBundle\Services\ImgService;
use Doctrine\ORM\Mapping as ORM;
/**
 * Type
 *
 * @ORM\Table(name="static_about")
 * @ORM\Entity
 */
class About
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="img", type="string", nullable=true)
     */
    private $img;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text", nullable=true)
     */
    private $meta_description;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="text", nullable=true)
     */
    private $meta_keywords;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    /**
     * @param string $img
     * @return About
     *
     * TODO rewrite to automatically upload file. It's costal, need because in easyadmin can not use path to saving image. vendor libraries do not works.
     */
    public function setImg(string $img = null): self
    {
        if($img) {
            $imgName = ImgService::upload($this->getUploadDir(), $img);
            $this->img = $imgName;
        }
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
    public function getUploadDir()
    {
        return 'static';
    }

    public function getMetaDescription(): ?string
    {
        return $this->meta_description;
    }

    public function setMetaDescription(?string $meta_description): self
    {
        $this->meta_description = $meta_description;

        return $this;
    }

    public function getMetaKeywords(): ?string
    {
        return $this->meta_keywords;
    }

    public function setMetaKeywords(?string $meta_keywords): self
    {
        $this->meta_keywords = $meta_keywords;

        return $this;
    }
}