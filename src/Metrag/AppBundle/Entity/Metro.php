<?php

namespace App\Metrag\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Type
 *
 * @ORM\Table(name="metro")
 * @ORM\Entity
 */
class Metro
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Metrag\AppBundle\Entity\Line", inversedBy="metro")
     * @ORM\JoinColumn(name="line_id", referencedColumnName="id")
     */
    private $line;

    /**
     * @ORM\ManyToOne(targetEntity="App\Metrag\AppBundle\Entity\District", inversedBy="metro")
     * @ORM\JoinColumn(name="district_id", referencedColumnName="id")
     * @Assert\NotNull
     */
    private $district;

    /**
     * @var integer
     *
     * @ORM\Column(name="position_on_line", type="integer", nullable=true)
     */
    private $position_on_line;

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLine(): ?Line
    {
        return $this->line;
    }

    public function setLine(?Line $line): self
    {
        $this->line = $line;

        return $this;
    }

    public function getPositionOnLine(): ?int
    {
        return $this->position_on_line;
    }

    public function setPositionOnLine(int $position_on_line): self
    {
        $this->position_on_line = $position_on_line;

        return $this;
    }

    public function getDistrict(): ?District
    {
        return $this->district;
    }

    public function setDistrict(?District $district): self
    {
        $this->district = $district;

        return $this;
    }
}