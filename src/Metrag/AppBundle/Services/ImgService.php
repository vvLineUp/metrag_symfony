<?php

namespace App\Metrag\AppBundle\Services;

use Symfony\Component\HttpFoundation\File\File;

class ImgService
{
    private const EXTENSIONS = [
        'image/jpeg' => 'jpg',
        'image/png' => 'png',
    ];

    private const UPLOAD_DIR = 'upload_img';

    public static function upload(string $uploadDir = '', string $from): ?string
    {

        $fileName = self::generateImgName() . '.' . self::convertMimeTypeToExtension($from);

        $file = new File($from);
        $file->move(
            'upload_img/' . $uploadDir,
            $fileName

        );

        $uploadDir = $uploadDir ? $uploadDir . '/' : '';
        return self::UPLOAD_DIR . '/' . $uploadDir . $fileName;
    }

    private static function generateImgName(): string
    {
        $randomStr = 'qwertyuioasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM123456789';
        $returnStr = '';
        $strLength = strlen($randomStr) - 1;

        for($i = 0; $i < 20; ++$i) {
            $returnStr .= $randomStr{rand(0, $strLength)};
        }
        return $returnStr;
    }

    private static function convertMimeTypeToExtension(string $pathToImg): string
    {
        return self::EXTENSIONS[mime_content_type($pathToImg)] ?? '';
    }

}