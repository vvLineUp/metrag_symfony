<?php

namespace App\Metrag\AppBundle\Services;

use Doctrine\Common\Persistence\ObjectManager;

class LivedComplexService
{
    /** @var ObjectManager */
    private $objectManager;

    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function getComplexes(): array
    {
        return $this->objectManager
            ->getRepository('AppBundle:LivedComplexStatic')
            ->findAll();
    }
}