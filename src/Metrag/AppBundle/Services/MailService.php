<?php

namespace App\Metrag\AppBundle\Services;

class MailService
{
    /** @var \Swift_Mailer */
    private $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function send(string $subject, string $from, string $to, string $body): bool
    {
        $message = (new \Swift_Message($subject))
            ->setFrom($from)
            ->setTo($to)
            ->setBody($body,'text/html');

        $failures = '';

        return (bool)$this->mailer->send($message, $failures);

    }
}