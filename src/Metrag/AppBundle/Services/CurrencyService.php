<?php

namespace App\Metrag\AppBundle\Services;

use Symfony\Component\Cache\Simple\FilesystemCache;

class CurrencyService
{
    /**
     * Get current course.
     *
     */
    public static function getCourse(): ?float
    {
        $cache = new FilesystemCache();
        try {
            $usd = $cache->get('currency_usd');
            if ($usd) {
                return (double)$usd;
            }

            $usd = self::getCoursesUSD();
            $cache->set('currency_usd', $usd, 36000);
            return (double)$usd;

        } catch(\Exception $exception) {
            return null;
        }
    }

    /**
     * Division of value.
     *
     * @param int $price
     * @return int
     */
    public static function priceToUsd(int $price): int
    {
        return intval(intval($price) / self::getCourse());
    }

    private static function getCoursesUSD(): string
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $coursesJson = json_decode(curl_exec($ch));

        curl_close($ch);
        return $coursesJson[0]->buy;
    }
}