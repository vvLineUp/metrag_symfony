<?php

// src/Metrag/AppBundle/DataFixtures/StateFixtures.php

namespace App\Metrag\AppBundle\DataFixtures;

use App\Metrag\AppBundle\Entity\Type;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TypeFixtrue extends BaseFixtrue
{
    public function load(ObjectManager $manager)
    {
        $type = new Type;
        $type->setId(1);
        $type->setName('Квартира');
        $this->disableGeneratorIdAndSave($manager, $type);

        $type = new Type;
        $type->setId(2);
        $type->setName('Дом');
        $this->disableGeneratorIdAndSave($manager, $type);

        $type = new Type;
        $type->setId(3);
        $type->setName('Участок');
        $this->disableGeneratorIdAndSave($manager, $type);

        $type = new Type;
        $type->setId(4);
        $type->setName('Коммерческая недвижимость');
        $this->disableGeneratorIdAndSave($manager, $type);
    }
}