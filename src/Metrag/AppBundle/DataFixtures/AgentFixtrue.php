<?php

// src/Metrag/AppBundle/DataFixtures/StateFixtures.php

namespace App\Metrag\AppBundle\DataFixtures;


use App\Metrag\AppBundle\Entity\Agent;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AgentFixtrue extends BaseFixtrue
{
    public function load(ObjectManager $manager)
    {
        $agent = new Agent;
        $agent->setId(1);
        $agent->setFullname('Метраж Юлия');
        $agent->setDescription('Description');
        $agent->setNumbers(['+380990999999','+380990999998','+380990999997']);
        $this->disableGeneratorIdAndSave($manager, $agent);

        $agent = new Agent;
        $agent->setId(2);
        $agent->setFullname('Метраж Ольга');
        $agent->setDescription('Description');
        $agent->setNumbers(['+380990999991','+380990999992','+380990999993']);
        $this->disableGeneratorIdAndSave($manager, $agent);
    }
}