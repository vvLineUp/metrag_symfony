<?php

// src/Metrag/AppBundle/DataFixtures/StateFixtures.php

namespace App\Metrag\AppBundle\DataFixtures;


use App\Metrag\AppBundle\Entity\RequestToAdmin;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class RequestOnViewFixtrue extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i < 300; ++$i) {
            $requestOnView = new RequestToAdmin;
            $requestOnView->setContacts('andrey@gmail.com/+380668999999');
            $requestOnView->setText('Запрос на просмотр ' . rand(1, 500));

            //random(show or not)
            $requestOnView->setIsShowing((bool)rand(0, 1));
            $manager->persist($requestOnView);
        }
        $manager->flush();
    }
}