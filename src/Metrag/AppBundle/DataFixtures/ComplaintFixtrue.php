<?php

// src/Metrag/AppBundle/DataFixtures/StateFixtures.php

namespace App\Metrag\AppBundle\DataFixtures;


use App\Metrag\AppBundle\Entity\Complaint;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ComplaintFixtrue extends Fixture
{
    public function load(ObjectManager $manager)
    {

        for ($i = 1; $i < 300; ++$i) {
            $complaint = new Complaint();
            $complaint->setContacts('andrey@gmail.com/+380668999999');
            $complaint->setText('Жалоба ' . rand(1, 500));
            $complaint->setComplaintOnId(1);
            $complaint->setType(1);
            $manager->persist($complaint);
        }

        $manager->flush();
    }
}