<?php

// src/Metrag/AppBundle/DataFixtures/StateFixtures.php

namespace App\Metrag\AppBundle\DataFixtures;


use App\Metrag\AppBundle\Entity\DealType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class DealTypeFixture extends BaseFixtrue
{
    public function load(ObjectManager $manager)
    {
        $dealType = new DealType();
        $dealType->setId(1);
        $dealType->setName('Новостройка');
        $this->disableGeneratorIdAndSave($manager, $dealType);
        $manager->persist($dealType);

        $manager->flush();
    }
}