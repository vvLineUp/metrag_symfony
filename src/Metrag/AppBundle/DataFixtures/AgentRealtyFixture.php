<?php

// src/Metrag/AppBundle/DataFixtures/StateFixtures.php

namespace App\Metrag\AppBundle\DataFixtures;

use App\Metrag\AppBundle\Entity\AgentRealty;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;

class AgentRealtyFixture extends BaseFixtrue implements DependentFixtureInterface
{
//    private $em;
//    public function __construct(EntityManager $entityManager)
//    {
//        $this->em = $entityManager;
//    }

    public function load(ObjectManager $manager)
    {

        $agent = $manager
            ->getRepository('AppBundle:Realty')
            ->find(21);

        //dd($agent);

//        $agent = $manager
//            ->getRepository('AppBundle:Agent')
//            ->find(1);
//
//        $realty = $manager
//            ->getRepository('AppBundle:Realty')
//            ->find(1);
//
//        //dd($agent);
//
//
//        $agentRealty = new AgentRealty;
//        $agentRealty->setAgent($agent);
//        $agentRealty->setRealty($realty);
//
//        $manager->persist($agentRealty);
//        $manager->flush();
    }
    public function getDependencies()
    {
        return [
            AgentFixtrue::class,
            DealTypeFixture::class,
            DistrictFixtrue::class,
            LayoutFixtrue::class,
            StateFixtrue::class,
            StatusFixtrue::class,
            TypeFixtrue::class,
            RealtyFixture::class,
        ];
    }
}