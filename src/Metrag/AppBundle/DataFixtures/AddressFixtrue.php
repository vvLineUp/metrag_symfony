<?php

// src/Metrag/AppBundle/DataFixtures/StateFixtures.php

namespace App\Metrag\AppBundle\DataFixtures;


use App\Metrag\AppBundle\Entity\Address;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AddressFixtrue extends BaseFixtrue
{
    public function load(ObjectManager $manager)
    {
        $address = new Address();
        $address->setId(1);
        $address->setName('megas@gmail.com');
        $this->disableGeneratorIdAndSave($manager, $address);

        $address->setId(2);
        $address->setName('address 1');
        $this->disableGeneratorIdAndSave($manager, $address);

        $address->setId(3);
        $address->setName('address 2');
        $this->disableGeneratorIdAndSave($manager, $address);
    }
}