<?php

// src/Metrag/AppBundle/DataFixtures/StateFixtures.php

namespace App\Metrag\AppBundle\DataFixtures;


use App\Metrag\AppBundle\Entity\Phone;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PhoneFixtrue extends BaseFixtrue
{
    public function load(ObjectManager $manager)
    {
        $phone = new Phone();
        $phone->setId(1);
        $phone->setPhone('099 999 99 99');
        $this->disableGeneratorIdAndSave($manager, $phone);

        $phone = new Phone();
        $phone->setId(2);
        $phone->setPhone('099 999 99 98');
        $this->disableGeneratorIdAndSave($manager, $phone);

        $phone = new Phone();
        $phone->setId(3);
        $phone->setPhone('099 999 99 97');
        $this->disableGeneratorIdAndSave($manager, $phone);

    }
}