<?php

// src/Metrag/AppBundle/DataFixtures/StateFixtures.php

namespace App\Metrag\AppBundle\DataFixtures;


use App\Metrag\AppBundle\Entity\About;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AboutFixtrue extends BaseFixtrue
{
    public function load(ObjectManager $manager)
    {
        $about = new About;
        $about->setId(1);
        $about->setDescription('Description text. Asdfg qwerty zxcvb');
        $about->setImg('about.jpeg');
        $this->disableGeneratorIdAndSave($manager, $about);
    }
}