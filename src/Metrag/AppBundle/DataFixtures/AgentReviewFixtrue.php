<?php

// src/Metrag/AppBundle/DataFixtures/StateFixtures.php

namespace App\Metrag\AppBundle\DataFixtures;

use App\Metrag\AppBundle\Entity\AgentReview;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AgentReviewFixtrue extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for($i = 1; $i < 10; ++$i) {
            $agent = $manager
                ->getRepository('AppBundle:Agent')
                ->find(rand(1,2));


            $district = new AgentReview();
            $district->setAgent($agent);
            $district->setText('text ' . $i);
            $district->setDatetime(new \DateTime());
            $manager->persist($district);
            $manager->flush();
        }

    }
    public function getDependencies()
    {
        return [
            AgentFixtrue::class,
        ];
    }
}