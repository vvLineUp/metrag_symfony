<?php

// src/Metrag/AppBundle/DataFixtures/StateFixtures.php

namespace App\Metrag\AppBundle\DataFixtures;


use App\Metrag\AppBundle\Entity\State;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class StateFixtrue extends BaseFixtrue
{
    public function load(ObjectManager $manager)
    {
        $state = new State;
        $state->setId(1);
        $state->setName('Новостройка');
        $this->disableGeneratorIdAndSave($manager, $state);

        $state = new State;
        $state->setId(2);
        $state->setName('Вторичка');
        $this->disableGeneratorIdAndSave($manager, $state);

        $state = new State;
        $state->setId(3);
        $state->setName('Элитная недвижимость');
        $this->disableGeneratorIdAndSave($manager, $state);
    }
}