<?php

// src/Metrag/AppBundle/DataFixtures/StateFixtures.php

namespace App\Metrag\AppBundle\DataFixtures;


use App\Metrag\AppBundle\Entity\District;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class DistrictFixtrue extends BaseFixtrue
{
    public function load(ObjectManager $manager)
    {
        $district = new District();
        $district->setId(1);
        $district->setName('Шевченковский');
        $this->disableGeneratorIdAndSave($manager, $district);

        $district->setId(3);
        $district->setName('Киевский');
        $this->disableGeneratorIdAndSave($manager, $district);

        $district->setId(4);
        $district->setName('Холодногорский');
        $this->disableGeneratorIdAndSave($manager, $district);


        $district->setId(5);
        $district->setName('Коминтерновский');
        $this->disableGeneratorIdAndSave($manager, $district);

        $district->setId(6);
        $district->setName('Московский');
        $this->disableGeneratorIdAndSave($manager, $district);

        $district->setId(7);
        $district->setName('Октябрьский');
        $this->disableGeneratorIdAndSave($manager, $district);

        $district->setId(8);
        $district->setName('Индустриальный');
        $this->disableGeneratorIdAndSave($manager, $district);

        $district->setId(9);
        $district->setName('Фрунзенский');
        $this->disableGeneratorIdAndSave($manager, $district);

        $district->setId(10);
        $district->setName('Червнонозаводский');
        $this->disableGeneratorIdAndSave($manager, $district);
    }
}