<?php

// src/Metrag/AppBundle/DataFixtures/StateFixtures.php

namespace App\Metrag\AppBundle\DataFixtures;

use App\Metrag\AppBundle\Entity\Realty;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;

class RealtyFixture extends BaseFixtrue implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        $district = $manager
            ->getRepository('AppBundle:District')
            ->find(1);

        $dealType = $manager
            ->getRepository('AppBundle:DealType')
            ->find(1);

        $type = $manager
            ->getRepository('AppBundle:Type')
            ->find(1);

        $status = $manager
            ->getRepository('AppBundle:Status')
            ->find(1);

        $state = $manager
            ->getRepository('AppBundle:State')
            ->find(1);

        $layout = $manager
            ->getRepository('AppBundle:Layout')
            ->find(9);

        $realty = new Realty();
        $realty->setId(1);
        $realty->setForeignId(1);
        $realty->setTypeId($type);
        $realty->setDatetime(new \DateTime());
        //$realty->setDistrict($district);
        $realty->setDistrict($district);
        $realty->setCoordinates(['52.379189', '4.899431']);
        $realty->setPrice(700);
        $realty->setDescription('Descr');
        $realty->setNumberInDatabase('0676854');
        $realty->setRooms(2);
        $realty->setSquare(35);
        $realty->setFloor(3);
        $realty->setFloorAll(11);
        $realty->setLayout($layout);

        $realty->setDealType($dealType);
        $realty->setIsCombinedBathroom(false);
        $realty->setStatus($status);
        $realty->setState($state);
        $realty->setBuiltYear(1990);
        $this->disableGeneratorIdAndSave($manager, $realty);
    }
    public function getDependencies()
    {
        return [
            AgentFixtrue::class,
            DealTypeFixture::class,
            DistrictFixtrue::class,
            LayoutFixtrue::class,
            StateFixtrue::class,
            StatusFixtrue::class,
            TypeFixtrue::class,
        ];
    }
}