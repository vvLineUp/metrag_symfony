<?php

// src/DataFixtures/StatusFixtures.php
namespace App\Metrag\AppBundle\DataFixtures;

use App\Metrag\AppBundle\Entity\Status;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class StatusFixtrue extends BaseFixtrue
{
    public function load(ObjectManager $manager)
    {

        $status = new Status;
        $status->setId(1);
        $status->setName('Топ');
        $this->disableGeneratorIdAndSave($manager, $status);

        $status = new Status;
        $status->setId(2);
        $status->setName('Премиум');
        $this->disableGeneratorIdAndSave($manager, $status);

        $status = new Status;
        $status->setId(3);
        $status->setName('Проданно');
        $this->disableGeneratorIdAndSave($manager, $status);
    }
}