<?php

namespace App\Metrag\AppBundle\DataFixtures;

use App\Metrag\AppBundle\Entity\News;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class NewsFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($i = 1; $i < 100; ++$i) {
            $news = new News;
            $news->setTitle('Title ' . rand(1, 10000));
            $news->setDescription('Description ' . rand(1, 10000));
            $news->setDatetime(new \DateTime());
            $news->setImg('/newses/1.jpg');
            $manager->persist($news);
        }

        $manager->flush();
    }
}