<?php

namespace App\Metrag\AppBundle\DataFixtures;

use App\Metrag\AppBundle\Entity\Layout;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LayoutFixtrue extends BaseFixtrue
{
    public function load(ObjectManager $manager)
    {
        $layout = new Layout;
        $layout->setId(1);
        $layout->setName('1+1');
        $this->disableGeneratorIdAndSave($manager, $layout);

        $layout = new Layout;
        $layout->setId(3);
        $layout->setName('2+1');
        $this->disableGeneratorIdAndSave($manager, $layout);

        $layout = new Layout;
        $layout->setId(4);
        $layout->setName('2+1+1');
        $this->disableGeneratorIdAndSave($manager, $layout);

        $layout = new Layout;
        $layout->setId(5);
        $layout->setName('2+2');
        $this->disableGeneratorIdAndSave($manager, $layout);

        $layout = new Layout;
        $layout->setId(6);
        $layout->setName('3+1');
        $this->disableGeneratorIdAndSave($manager, $layout);

        $layout = new Layout;
        $layout->setId(7);
        $layout->setName('3+2');
        $this->disableGeneratorIdAndSave($manager, $layout);

        $layout = new Layout;
        $layout->setId(8);
        $layout->setName('4+1');
        $this->disableGeneratorIdAndSave($manager, $layout);

        $layout = new Layout;
        $layout->setId(9);
        $layout->setName('разд');
        $this->disableGeneratorIdAndSave($manager, $layout);

        $layout = new Layout;
        $layout->setId(10);
        $layout->setName('расп');
        $this->disableGeneratorIdAndSave($manager, $layout);

        $layout = new Layout;
        $layout->setId(11);
        $layout->setName('свобод');
        $this->disableGeneratorIdAndSave($manager, $layout);

        $layout = new Layout;
        $layout->setId(12);
        $layout->setName('смеж');
        $this->disableGeneratorIdAndSave($manager, $layout);

        $layout = new Layout;
        $layout->setId(13);
        $layout->setName('студия');
        $this->disableGeneratorIdAndSave($manager, $layout);
    }
}