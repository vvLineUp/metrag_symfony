<?php

// src/Metrag/NewsBundle/Controller/NewsController.php

namespace App\Metrag\NewsBundle\Controller;

use App\Metrag\ApiBundle\Transformers\NewsTransformer;
use App\Metrag\AppBundle\Entity\News;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class NewsController extends Controller
{

    public function indexAction(Request $request)
    {
        return $this->render('@News/news/index.html.twig',[
            'days' => $request->query->getInt('days', 0)
        ]);
    }


    public function showAction(News $news)
    {
        return $this->render('@News/news/show.html.twig',[
            'news' => (new NewsTransformer)->transform([$news])[0],
        ]);
    }

}