<?php

// src/Metrag/AgentBundle/Controller/ReviewController.php
namespace App\Metrag\AgentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Metrag\AppBundle\Entity\Agent;
use Symfony\Component\HttpFoundation\Request;

class ReviewController extends Controller
{
    public function indexAction(Request $request)
    {
        return $this->render('@Agent/review/index.html.twig');
    }
}