<?php
// src/Metrag/RealtyBundle/RealtyController.php

namespace App\Metrag\RealtyBundle\Controller;

use App\Metrag\AgentBundle\Services\AgentService;
use App\Metrag\ApiBundle\Transformers\AgentTransformer;
use App\Metrag\ApiBundle\Transformers\RealtyTransformer;
use App\Metrag\AppBundle\Entity\Realty;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Metrag\ApiBundle\Services\RealtyService;
use Thormeier\BreadcrumbBundle\Model\Breadcrumb;


/**
 * Render html blocks. One method - one html block
 *
 * Class HtmlController
 * @package App\Metrag\RealtyBundle\Controller
 */
class RealtyController extends Controller
{
    public function printAction(Realty $realty, AgentService $agentService)
    {
        return $this->render('@App/realty/print.html.twig', [
            'realty' => (new RealtyTransformer())->transform([$realty])[0],
            'agents' => (new AgentTransformer())->transform($agentService->getAgentOfRealty($realty)),
        ]);
    }

    public function showAction(Realty $realty, RealtyService $realtyService, AgentService $agentService)
    {
        return $this->render('@App/realty/show.html.twig', [
            'realty' => (new RealtyTransformer())->transform([$realty])[0],
            'realties_similar' => (new RealtyTransformer())->transform($realtyService->getSimilar($realty)),
            'agents' => (new AgentTransformer())->transform($agentService->getAgentOfRealty($realty)),
        ]);
    }

    private function setUpBreadcrumps(Realty $realty, Breadcrumb $breadcrumb)
    {
        $breadcrumbProvider = $this->get('thormeier.breadcrumb.breadcrumb_provider');
        $crumb = $breadcrumbProvider->getBreadcrumbByRoute('realty_show');
        $crumb->setRouteParams([ 'id' => $realty->getId(), ]);
    }

}
