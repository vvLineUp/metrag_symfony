<?php
// src/Metrag/RealtyBundle/SearchController.php

namespace App\Metrag\RealtyBundle\Controller;

use App\Metrag\ApiBundle\Services\RealtyService;
use App\Metrag\ApiBundle\Transformers\RealtyTransformer;
use App\Metrag\AppBundle\Entity\Realty;
use App\Metrag\RealtyBundle\Transformers\CoordinateTransformer;
use Doctrine\ORM\Query\ResultSetMapping;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;



class SearchController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchAction(Request $request)
    {


        #$this->server->set('REQUEST_URI', '/en/guestbook');
        #dd($request->set('REQUEST_URI', '/en/guestbook'));


//        $this->generateUrl('search', ['slug' => $slug]);
//        $this->get('router')->generate('search', array(
//            'slug' => $slug
//        ));

        //
//        $new_arr = [];
//        if ($deal_type) {
//            $new_arr['deal_type'] = $deal_type;
//        }
//
//        if ($type) {
//            $new_arr['type'] = $request->query->get('type' );
//        }
//
//        $new = http_build_query($new_arr,'','-');

        // $deal_type = $request->query->get('deal_type' );
        // $type = $request->query->get('type' );

        // $slug = '';
        // if ($deal_type && $deal_type == 1) {
        //     $slug .= 'arenda';
        // } elseif ($deal_type && $deal_type == 2) {
        //     $slug .= 'kupit';
        // } else {
        //     $slug = 'nedvigimost';
        // }

        // if ($type) {
        //     switch ($type) {
        //         case 1:
        //             $slug .= '-kvartiry';
        //             break;
        //         case 2:
        //             $slug .= '-novostroika';
        //             break;
        //         case 3:
        //             $slug .= '-ychastok';
        //             break;
        //         case 4:
        //             $slug .= '-kommercial_nedvigimost';
        //             break;
        //         case 5:
        //             $slug .= '-dom';
        //             break;
        //     }
        // }


        #$request->attributes->set('slug', $slug);
        #$request->attributes->set('_route_params', ['slug' => $slug]);

        return $this->render('@Realty/search/search.html.twig', [
                #'slug' => $slug
        ]);
    }
    /**
     * @param Request $request
     * @param RealtyService $realtyService
     * @return JsonResponse
     * TODO rewrite using vue.js
     */
    public function getRealtiesAction(Request $request, RealtyService $realtyService)
    {
        $realties = $realtyService->getRealties($request);
        return new JsonResponse($realties);
    }

}