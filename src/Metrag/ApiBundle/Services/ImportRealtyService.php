<?php

namespace App\Metrag\ApiBundle\Services;

use App\Metrag\AppBundle\Entity\DealType;
use App\Metrag\AppBundle\Entity\Agent;
use App\Metrag\AppBundle\Entity\LivedComplex;
use App\Metrag\AppBundle\Entity\Realty;
use App\Metrag\AppBundle\Entity\Street;
use App\Metrag\AppBundle\Entity\Type;
use App\Metrag\AppBundle\Helpers\AddressToLatLngHelper;
use Doctrine\Common\Persistence\ObjectManager;

class ImportRealtyService
{
    const MAX_ON_PAGE = 100;

    /*
     * !!!Parameters on default: page=0.
     * For test: https://metrag.com.ua/site/info?type=apartments&page=0
     */
    const URLS = [
        'apartments' => 'https://admin.metrag.com.ua/site/info?type=apartments',
        'houses' => 'https://admin.metrag.com.ua/site/info?type=houses',
        'areas' => 'https://admin.metrag.com.ua/site/info?type=areas',
        'commercials' => 'https://admin.metrag.com.ua/site/info?type=commercials',
        'building' => 'https://admin.metrag.com.ua/site/info?type=building',
    ];

    const AGENT_URLS = 'https://admin.metrag.com.ua/site/agents';

    const COMPLEX_URL = 'https://admin.metrag.com.ua/site/complexes';

    private $objectManager;

    //ids only for display one site.
    private $processedIds;

    private $processedAgentsIds;

    private $processedComplexesIds;

    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function importRealty(): bool
    {
        $this->importAgentsOnUrl();

        //$this->importComplexesOnUrl();

        $this->saveRealtiesOnUrl(self::URLS['apartments'] . '&page=', 1);
        $this->saveRealtiesOnUrl(self::URLS['houses'] . '&page=', 1);
        $this->saveRealtiesOnUrl(self::URLS['areas'] . '&page=', 1);
        $this->saveRealtiesOnUrl(self::URLS['commercials'] . '&page=', 1);
        $this->saveRealtiesOnUrl(self::URLS['building'] . '&page=', 1);
        if (!empty($this->processedIds)) {
            $this->removeNotInList($this->processedIds);
        }

        return true;
    }

    private function importAgentsOnUrl(): void
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::AGENT_URLS);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);

        $agents = json_decode($response);


        if($agents) {
            $this->saveAgents($agents);

            if (!empty($this->processedAgentsIds)) {
                $this->removeAgents($this->processedAgentsIds);
            }
        }


        return;
    }

//    private function importComplexesOnUrl(): void
//    {
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, self::COMPLEX_URL);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//
//        $response = curl_exec($ch);
//
//        $complexes = json_decode($response);
//
//        $this->saveComplexes($complexes);
//
//        if (!empty($this->processedComplexesIds)) {
//            $this->removeComplexes($this->processedComplexesIds);
//        }
//
//        return;
//    }

//    private function saveComplexes(array $complexesForeign)
//    {
//        foreach ($complexesForeign as $complexForeign) {
//            $livedComplex = $this->objectManager
//                ->getRepository('AppBundle:LivedComplex')
//                ->find($complexForeign->id);
//
//            if (!$livedComplex) {
//                $livedComplex = new LivedComplex();
//                $livedComplex->setId($complexForeign->id);
//            }
//
//            $livedComplex->setName($complexForeign->name);
//
//            $manager = $this->objectManager;
//            $metadata = $manager->getClassMetadata(get_class($livedComplex));
//            $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());
//            $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
//            $manager->persist($livedComplex);
//            $manager->flush();
//
//            $this->processedAgentsIds[] = $complexForeign->id;
//        }
//    }

    /**
     * @param array $removeList
     * @return mixed
     * TODO rewrite to on delete cascade
     */
//    private function removeComplexes(array $removeList)
//    {
//        $sql = 'DELETE FROM lived_complex WHERE lived_complex .id NOT IN(' . implode(',', $removeList) . ')';
//        $stmt = $this->objectManager->getConnection()->prepare($sql);
//        $stmt->execute();
//
//        return $this->objectManager
//            ->getRepository('AppBundle:LivedComplex')
//            ->removeWhereIdsNotIn($removeList);
//    }


    private function saveAgents(array $agentsForeign)
    {
        foreach ($agentsForeign as $agentForeign) {
            $agent = $this->objectManager
                ->getRepository('AppBundle:Agent')
                ->find($agentForeign->id);

            if (!$agent) {
                $agent = new Agent;
                $agent->setId($agentForeign->id);
            }

            $agent->setFullname($agentForeign->name);
            $agent->setDescription($agentForeign->description);
            $agent->setNumbers($agentForeign->numbers);

            $manager = $this->objectManager;
            $metadata = $manager->getClassMetadata(get_class($agent));
            $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());
            $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
            $manager->persist($agent);
            $manager->flush();

            $this->processedAgentsIds[] = $agentForeign->id;
        }
    }

    /**
     * @param array $removeList
     * @return mixed
     * TODO rewrite to on delete cascade
     */
    private function removeAgents(array $removeList)
    {
        $sql = 'DELETE FROM agent_realties WHERE agent_realties.agent_id NOT IN(' . implode(',', $removeList) . ')';
        $stmt = $this->objectManager->getConnection()->prepare($sql);
        $stmt->execute();

        return $this->objectManager
            ->getRepository('AppBundle:Agent')
            ->removeWhereIdsNotIn($removeList);
    }

    public function importStreet(string $streetName)
    {
        $currentStreet = $this->objectManager
            ->getRepository('AppBundle:Street')
            ->findByName($streetName);

        if ($currentStreet) {
            return;
        }

        $street = new Street;
        $street->setName($streetName);
        $this->objectManager->persist($street);
        $this->objectManager->flush();
    }

    private function saveRealtiesOnUrl(string $url, $page = 1): void
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . $page);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json')); // Assuming you're requesting JSON
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);

        $realtiesData = json_decode($response);
        if (!$realtiesData) {
            return;
        }

        $this->saveRealty($realtiesData);

        //if is not last page, run
        if (count($realtiesData) === self::MAX_ON_PAGE) {
            $this->saveRealtiesOnUrl($url, $page + 1);
        }
        return;
    }

    private function getBuildingSubtypeOnState(?int $subtype): ?int
    {
        if ($subtype === 12 || $subtype === 6 || $subtype === 5) {
            return 12;
        } elseif ($subtype === 14 || $subtype === 7) {
            return 11;
        }
        return null;
    }

    private function saveRealty(array $realtiesForeign)
    {
        foreach ($realtiesForeign as $realtyForeign) {
            $dealTypeId = !empty($realtyForeign->deal_type_id) ? $realtyForeign->deal_type_id : DealType::TYPES['BUY'];

            if($realtyForeign->description !== '' && $this->isSimilarRealty($realtyForeign->realty_type_id, $dealTypeId, $realtyForeign->description, $realtyForeign->foreign_id)) {
                echo 'Double: ' . $realtyForeign->foreign_id . "\n\r";
                continue;
            }

            $realty = $this->getForeignRealty($realtyForeign->foreign_id, $realtyForeign->realty_type_id, $dealTypeId) ?? new Realty;

            $district = !empty($realtyForeign->district_id) ? $this->objectManager
                ->getRepository('AppBundle:District')
                ->find($realtyForeign->district_id) : null;

            $type = !empty($realtyForeign->realty_type_id) ? $this->objectManager
                ->getRepository('AppBundle:Type')
                ->find($realtyForeign->realty_type_id) : null;


            $dealType = $this->objectManager
                ->getRepository('AppBundle:DealType')
                ->find($dealTypeId);

            $status = !empty($realtyForeign->status_id) ? $this->objectManager
                ->getRepository('AppBundle:Status')
                ->find((int)($realtyForeign->status_id)) : null;

            $state = !empty($realtyForeign->state_id) ? $this->objectManager
                ->getRepository('AppBundle:State')
                ->find($realtyForeign->state_id) : null;

            $layout = !empty($realtyForeign->layout_id) ? $this->objectManager
                ->getRepository('AppBundle:Layout')
                ->find((int)$realtyForeign->layout_id) : null;

            $metro = !empty($realtyForeign->metro_id) ? $this->objectManager
                ->getRepository('AppBundle:Metro')
                ->find((int)$realtyForeign->metro_id) : null;

            $livedComplex = !empty($realtyForeign->lived_complex_id) ? $this->objectManager
                ->getRepository('AppBundle:LivedComplex')
                ->find((int)$realtyForeign->lived_complex_id) : null;

            $realtyForeign->state_id = $this->getBuildingSubtypeOnState($realtyForeign->state_id);

            if ($realtyForeign->realty_type_id === Type::TYPES['building']) {
                $realtyForeign->sub_type_id = $this->getBuildingSubtypeOnState($realtyForeign->condit_id);
            }

            $subType = !empty($realtyForeign->sub_type_id) ? $this->objectManager
                ->getRepository('AppBundle:SubType')
                ->find((int)$realtyForeign->sub_type_id) : null;


            $foreignBathroomType = isset($realtyForeign->bathroom_type) ? (int)$realtyForeign->bathroom_type : 0;
            $foreignBathroomType = $foreignBathroomType !== 0 ? $foreignBathroomType : 2;

            $bathroomType = $this->objectManager
                ->getRepository('AppBundle:BathroomType')
                ->find($foreignBathroomType);

            $realty->setForeignId($realtyForeign->foreign_id ?? null);
            $realty->setType($type);

            $realty->setDatetime(new \DateTime($realtyForeign->date));

            $realty->setDistrict($district ?? null);

            $coords = $realty->getCoordinates();

            if($realty->getStreet() !== $realtyForeign->street || $realty->getNumberBulding() !== $realtyForeign->number_building || empty($coords['lat'])) {

                $coordinates = AddressToLatLngHelper::convert($realtyForeign->street . ' ' . $realtyForeign->number_building);
                $realty->setCoordinates(['lat' => floatval($coordinates['lat']) ?? 0, 'lng' => floatval($coordinates['lng']) ?? 0]);

                $realty->setStreet($realtyForeign->street);
                $realty->setNumberBulding($realtyForeign->number_building);
            }

            $realty->setPrice($realtyForeign->price ?? null);
            $realty->setDescription($realtyForeign->description ?? '');

            //type and id from db
            $realty->setBathroomType($bathroomType);
            $realty->setRooms($realtyForeign->rooms ?? 0);
            $realty->setSquare($realtyForeign->total_area ?? null);
            $realty->setSquareLiving($realtyForeign->square_living ?? null);
            $realty->setSquareKitchen($realtyForeign->square_kitchen ?? null);
            $realty->setSquarePlot($realtyForeign->square_plot ?? null);
            $realty->setFloor($realtyForeign->floor ?? null);
            $realty->setFloorAll($realtyForeign->floor_all ?? null);
            $realty->setLayout($layout);
            $realty->setStatus($status);
            $realty->setState($state);
            $realty->setDealType($dealType);
            $realty->setIsCombinedBathroom(false);
            $realty->setBuiltYear($realtyForeign->year_built ?? 0);
            $realty->setCountBalcony($realtyForeign->count_balcony ?? null);
            $realty->setCountBalconyGlasses($realtyForeign->count_balcony_glazed ?? null);
            $realty->setMetro($metro);
            $realty->setSubType($subType);
            $realty->setLivedComplex($livedComplex);
            $realty->setImages($realtyForeign->images);

            $realty->removeAllAgents();

            $this->objectManager->persist($realty);
            $this->objectManager->flush();

            $this->addAgentsToRealty($realty, $realtyForeign->agents);

            $this->processedIds[] = $realty->getId();
        }
    }

    private function isSimilarRealty($realtyTypeId, $dealTypeId, $description, $foreignId): bool
    {
        $realtyOnCriteries = $this->objectManager
            ->getRepository('AppBundle:Realty')
            ->findOneBy([
                'description' => $description,
                'type' => $realtyTypeId,
                'deal_type' => $dealTypeId
            ]);

        return !empty($realtyOnCriteries) && $realtyOnCriteries->getForeignId() !== $foreignId;
    }

    private function addAgentsToRealty(Realty $realty, array $agents)
    {
        foreach ($agents as $agent) {
            if (!$agent) {
                continue;
            }

            $agentObject = $this->objectManager
                ->getRepository('AppBundle:Agent')
                ->find($agent);

            if ($agentObject) {
                $realty->addAgent($agentObject);
            }

            $this->objectManager->persist($realty);
            $this->objectManager->flush();
        }
    }

    private function getForeignRealty(int $foreignId, int $typeId, int $dealTypeId): ?Realty
    {
        return $this->objectManager
            ->getRepository('AppBundle:Realty')
            ->findOneBy([
                'foreign_id' => $foreignId,
                'type' => $typeId,
                'deal_type' => $dealTypeId
            ]);
    }

    /**
     * @param array $realties
     * @return bool
     * TODO delete in ManyToMany annotation.
     */
    private function removeNotInList(array $realties): bool
    {
        $sql = 'DELETE FROM agent_realties WHERE agent_realties.realty_id NOT IN(' . implode(',', $realties) . ')';
        $stmt = $this->objectManager->getConnection()->prepare($sql);
        $stmt->execute();

        return $this->objectManager
            ->getRepository('AppBundle:Realty')
            ->removeWhereIdsNotIn($realties);
    }

}
