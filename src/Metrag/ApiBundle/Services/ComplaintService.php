<?php

namespace App\Metrag\ApiBundle\Services;

use App\Metrag\AppBundle\Entity\Complaint;
use Doctrine\Common\Persistence\ObjectManager;

class ComplaintService
{
    public const COMPLAINT_TYPES = [
        'TYPE_REALTY' => 1
    ];

    private $objectManager;

    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function store(string $fullname, string $contacts, string $text, string $id, string $requestType)
    {
        $requestToAdmin = new Complaint();
        $requestToAdmin->setFullname($fullname);
        $requestToAdmin->setContacts($contacts);
        $requestToAdmin->setText($text);
        $requestToAdmin->setComplaintOnId($id);
        $requestToAdmin->setType($requestType);

        $this->objectManager
            ->persist($requestToAdmin);
        $this->objectManager->flush();
    }

}