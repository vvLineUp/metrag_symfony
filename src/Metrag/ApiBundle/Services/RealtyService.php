<?php

namespace App\Metrag\ApiBundle\Services;

use App\Metrag\ApiBundle\Transformers\RealtyTransformer;
use App\Metrag\AppBundle\Entity\Currency;
use App\Metrag\AppBundle\Entity\DealType;
use App\Metrag\AppBundle\Entity\Realty;
use App\Metrag\AppBundle\Entity\SeoPage;
use App\Metrag\AppBundle\Helpers\PaginationHelper;
use App\Metrag\AppBundle\Services\CurrencyService;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Query;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;

class RealtyService
{
    /** @var ObjectManager */
    private $objectManager;

    private $paginator;

    private $templating;

    public function __construct(ObjectManager $objectManager, PaginatorInterface $paginator, \Twig_Environment $templating)
    {
        $this->objectManager = $objectManager;
        $this->paginator = $paginator;
        $this->templating = $templating;
    }

    public function getOnStatus(array $status, int $limit = 30): array
    {
        return $this->objectManager
            ->getRepository('AppBundle:Realty')

            ->findBy(
                [
                    'status' => $status
                ],
                ['datetime' => 'DESC'],
                $limit
            );
    }

    public function getRealties(Request $request, $var = null)
    {
        $realtiesOnPage = getenv('SEARCH_REALTIES_LIMIT');

        if ($request->getPathInfo() == "/seo-page-realties") {

            $page = $this->objectManager
                ->getRepository('AppBundle:SeoPage')
                ->findBy(
                    [
                        'url_alias' => $var,
                    ]
                );

            $query_string = $page[0]->getUrl();
            $query_string = preg_replace('/%5B%5D/', '', $query_string);

            $params_for_seo = $this->getStringToArray($query_string);

            foreach ($params_for_seo as $key => $value) {
                $request->query->set($key, $value);
            }
        }

        $params = $request->query->all();

        //in api
        $query = $this->getRealtyOnFilter($params);

        $allRealties = $this->getRealtyOnFilter($request->query->all())->execute();

        $pagination = $this->paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            getenv('SEARCH_REALTIES_LIMIT')
        );

        $realties = $pagination->getItems();

        $transformedRealties = (new RealtyTransformer())->transform($realties, $params);

        $totalCount = $pagination->getTotalItemCount();

        $html = $realties ?
            $this->templating->render('@Realty/search/_search_house.html.twig', [
                'realties' => $transformedRealties,
            ]) :
            $this->templating->render('@Realty/search/_search_not_found.html.twig');

        return [
            'totalCount' => $pagination->getTotalItemCount(),
            'page' => $pagination->getCurrentPageNumber(),
            'html' => $html,
            'count_pages' => PaginationHelper::getCounPages($totalCount, $realtiesOnPage),
            'coordinates' => $this->getOnlyMarkerData((new RealtyTransformer())->transform($allRealties, $params)),
        ];
    }

    public function getStringToArray($string, $delimiter = '&', $kv = '=')
    {
        if ($parts = explode($delimiter, $string)) { // create parts
            foreach ($parts as  $i => $val ) { // each part
                if ($val) {
                    if ($pos = strpos($val, $kv)) { // key/value delimiter
                        $res[trim(substr($val, 0, $pos))] = trim(substr($val, $pos + strlen($kv)));
                         #if (array_key_exists('district' , $res)) {
                          #   var_dump($val);
                             //todo create new array disctinct
                         #}
                    } else { // key delimiter not found
                        $res[] = trim($val);
                    }
                }
            }
            return $res;
        }


    }

    private function getOnlyMarkerData(array $realties)
    {
        $markerData = [];

        /** @var Realty $realty */
        foreach($realties as $realty) {

            $coordinates = $realty['coordinates'];
            if(!$coordinates['lat']) {
                continue;
            }

            $markerData[] = [
                'id' => $realty['id'],
                'price' => $realty['price'],
                'img' => $realty['first_img'],
                'type' => 'type' . $realty['type_id'],
                'square' => $realty['square'],
                'square_living' => $realty['square_living'],
                'square_kitchen' => $realty['square_kitchen'],
                'square_plot' => $realty['square_plot'],
                'floor' => $realty['floor'],
                'floor_all' => $realty['floor_all'],
                'lat' => $coordinates['lat'],
                'lng' => $coordinates['lng']
            ];
        }
        return $markerData;
    }

    public function getSimilar(Realty $realty)
    {
        $rooms = $realty->getRooms();
        $district = $realty->getDistrict();
        $districtId = $district ? $district->getId() : null;

        $filters = [
            'rooms' => $rooms,
            'type' => $realty->getType()->getId(),
            'deal_type' => $realty->getDealType()->getId()
        ];

        if($districtId) {
            $filters += ['district' => $districtId];
        }

        $realties = $this->objectManager
            ->getRepository('AppBundle:Realty')
            ->getByFiltersAndNotCurrentId($filters, $realty->getId());

        return $realties;

    }

    private function getRealtiesIdsNotMainAgent(): array
    {
        $sql = 'SELECT realty_id FROM agent_realties AS ar WHERE ar.agent_id <> ' . (int)getenv('DEFAULT_AGENT');
        $stmt = $this->objectManager->getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        foreach($result as $i => $item) {
            $result[$i] = $item['realty_id'];
        }

        return $result;
    }

    public function getRealtyOnFilter(array $filters): Query
    {
        //if price UAH, then to usd
        if(isset($filters['currency']) && $this->isUahPrice((int)$filters['currency'])) {
            $filters['price_from'] = CurrencyService::priceToUsd($filters['price_from']);
            $filters['price_to'] = CurrencyService::priceToUsd($filters['price_to']);
        }

        $data['not_main_agent_realties_ids'] = $this->getRealtiesIdsNotMainAgent();

        return $this->objectManager
            ->getRepository('AppBundle:Realty')
            ->getByFiltersQuery($filters, $data);

    }

    private function isUahPrice(int $currency): bool
    {
        return Currency::CURRENCY_NAME['UAH']['index'] === (int)$currency;
    }

}