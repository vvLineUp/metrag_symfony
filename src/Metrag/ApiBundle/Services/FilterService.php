<?php

namespace App\Metrag\ApiBundle\Services;

use Doctrine\Common\Persistence\ObjectManager;

class FilterService
{

    private $objectManager;

    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    public function getMetroOnLineId(int $lineId): array
    {
        return $this->objectManager
            ->getRepository('AppBundle:Metro')
            ->findBy(['line' => $lineId], ['position_on_line' => 'DESC']);
    }
}