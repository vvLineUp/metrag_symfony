<?php

namespace App\Metrag\ApiBundle\Transformers;

use App\Metrag\AppBundle\Entity\AgentReview;
use App\Metrag\AppBundle\Helpers\DateHelper;

class ReviewTransformer
{
    public function transform(array $reviews): array
    {
        $response = [];
        /** @var AgentReview $review */
        foreach ($reviews as $review) {
            $response[] = [
                'id' => $review->getId(),
                'agent' => (new AgentTransformer())->transform([$review->getAgent()])[0],
                'text' => $review->getText(),
                'date' => DateHelper::toShowing($review->getDatetime()),
            ];
        }

        return $response;
    }
}