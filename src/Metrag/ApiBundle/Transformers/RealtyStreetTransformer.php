<?php

namespace App\Metrag\ApiBundle\Transformers;

use App\Metrag\AppBundle\Entity\Realty;
use App\Metrag\AppBundle\Entity\Street;

class RealtyStreetTransformer
{
    public function transform(array $realties): array
    {

        $streets = [];
        /** @var Realty $realty */
        foreach ($realties as $realty) {

            /** @var Street $street */
            $street = $realty->getStreet();

            //dd($street);

            //if(!$realty->getIsActive() || !$street || in_array($street->getId(), $streets)) {
            //    continue;
            //}
            if(!in_array($street, $streets)) {
                $streets[] = $street;
                
            }
        }

        return $streets;
    }
}