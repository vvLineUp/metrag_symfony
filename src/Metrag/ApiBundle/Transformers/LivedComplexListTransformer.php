<?php

namespace App\Metrag\ApiBundle\Transformers;

use App\Metrag\AppBundle\Entity\LivedComplex;

class LivedComplexListTransformer
{
    public function transform(array $livedComplexes): array
    {
        $response = [];

        /** @var LivedComplex $livedComplex */
        foreach($livedComplexes as $livedComplex) {
            $response[] = [
                'id' => $livedComplex->getId(),
                'name' => $livedComplex->getName(),
            ];
        }

        return $response;
    }
}
