<?php

namespace App\Metrag\ApiBundle\Transformers;

use App\Metrag\AppBundle\Entity\Agent;

class AgentTransformer
{
    public function transform(array $agents): array
    {
        $response = [];

        /**
         * Agent $agent
         */
        foreach($agents as $agent) {
            $response[] = [
                'id' => $agent->getId(),
                'fullname' => $agent->getFullName(),
                'numbers' => $agent->getNumbers(),
                'description' => $agent->getDescription(),
                'img' => $agent->getImg()
            ];
        }

        return $response;
    }
}