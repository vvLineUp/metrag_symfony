<?php

namespace App\Metrag\ApiBundle\Transformers;

use App\Metrag\AppBundle\Entity\Currency;
use App\Metrag\AppBundle\Entity\DealType;
use App\Metrag\AppBundle\Entity\Realty;
use App\Metrag\AppBundle\Helpers\DateHelper;
use App\Metrag\AppBundle\Services\CurrencyService;

class RealtyTransformer
{
    public function transform(array $realties, $requestParams = []): array
    {
        $currency = !empty($requestParams['currency']) ? (int)$requestParams['currency']: null;
        $response = [];
        /** @var Realty $realty */
        foreach ($realties as $realty) {
            if(!$realty->getIsActive()) {
                continue;
            }
            $meta_description = $this->getMetaInfo($realty, $realty->getMetaDescription());

            $meta_title = $this->getMetaInfo($realty, $realty->getMetaTitle());

            $response[] = [
                'id' => $realty->getId(),
                'foreign_id' => $realty->getForeignId(),
                'type_id' => $realty->getType()->getId(),
                'type' => $realty->getTypeName(),
                'deal_type' => $realty->getDealType(),
                'description' => $realty->getDescription(),
                'district' => $realty->getDistrictName(),
                'coordinates' => $realty->getCoordinates(),
                'price' => $this->transformPrice($realty->getPrice(), CurrencyService::getCourse(), $realty->getDealType()->getId(), $currency),
                'number_in_database' => $realty->getForeignId(),
                'rooms' => $realty->getRooms(),
                'floor' => $realty->getFloor(),
                'floor_all' => $realty->getFloorAll(),
                'square' => $realty->getSquare(),
                'square_living' => $realty->getSquareLiving(),
                'square_kitchen' => $realty->getSquareKitchen(),
                'square_plot' => $realty->getSquarePlot(),
                'is_combined_bathroom' => $realty->getIsCombinedBathroom(),
                'bathroom_type' => $realty->getBathroomTypeName(),
                'bathroom_type_id' => $realty->getBathroomTypeName(),
                'layout' => $realty->getLayoutName(),
                'status' => $realty->getStatusName(),
                'state' => $realty->getStateName(),
                'built_year' => $realty->getBuiltYear(),
                'count_balcony' => $realty->getCountBalcony(),
                'count_balcony_glasses' => $realty->getCountBalconyGlasses(),
                'images' => $realty->getImages(),
                'metro' => $realty->getMetroName(),
                'street' => $realty->getStreet(),
                'datetime' => DateHelper::toShowing($realty->getDatetime()),
                'first_img' => $realty->getFirstImg(),
                'lived_complex' => $realty->getLivedComplexName(),
                'count_imgs' => $realty->getCountImgs(),
                'title' => $realty->getTitle(),
                'meta_description' => $meta_description,
                'meta_title' => $meta_title,
                'meta_keywords' => $realty->getMetaKeywords(),
            ];
        }

        return $response;
    }

    private function transformPrice(int $price, float $course = null, int $dealType, ?int $currency): string
    {
            if($dealType === DealType::TYPES['RENT']) {
            return $price .  ' грн';
        }

        if($currency === Currency::CURRENCY_NAME['UAH']['index'] && $course) {
            return round(intval($price * $course), -3) .  ' грн';
        }

        return intval($price) . ' $';
    }

    /**
     * @param $realty
     * @param $meta_tag
     * @return string
     */
    private function getMetaInfo($realty, $meta_tag): string
    {
        if (!empty($meta_tag)) {
            $items = [];
            $info = '';
            foreach (explode(',', $meta_tag) as $value) {
                $value = trim($value);
                if($value[0] != "{") {
                    return $meta_tag;
                } else {
                    $items[] = trim($value, '{,}');
                }
            }
            foreach ($items as $item) {
                switch ($item) {
                    case 'тип сделки':
                        $deal_type = $realty->getDealType()->getName();
                        $info .= $deal_type;
                        break;
                    case 'улица':
                        $street = $realty->getStreet();
                        $info .= ', '.$street;
                        break;
                    case 'номер в базе':
                        $id = $realty->getId();
                        $info .= ', '.$id;
                        break;
                    case 'количество комнат':
                        $rooms = $realty->getRooms();
                        $info .= ', количество комнат - '.$rooms;
                        break;
                    case 'площадь':
                        $square = $realty->getSquare();
                        $info .= ', площадь - '.$square. ' м2';
                        break;
                    case 'жилой комплекс':
                        $lived_complex = $realty->getLivedComplex() ? $realty->getLivedComplex()->getName() : '';
                        $info .= $lived_complex ? ', жилой комплекс - '.$lived_complex : '';
                        break;
                    default:
                        $info = '';
                        break;
                }
            }
            return $info;
        }
        return '';
    }

}