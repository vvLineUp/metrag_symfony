<?php

// src/Metrag/ApiBundle/Controller/LetterController.php
namespace App\Metrag\ApiBundle\Controller;

use App\Metrag\ApiBundle\Services\MessageService;
use App\Metrag\AppBundle\Helpers\DateHelper;
use App\Metrag\AppBundle\Services\MailService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class LetterController extends Controller
{
    public function reviewAction(Request $request, MailService $mailService, MessageService $messageService)
    {
        if($messageService->canNotWrite()) {
            return new JsonResponse([
                'error' => $this->get('translator')->trans('To much requests. Max once in three minutes')
            ], 400);
        }

        $fullName = $request->request->get('fullname', '');
        $contacts = $request->request->get('contacts', '');
        $text = $request->request->get('text', '');

        $message = $this->get('translator')->trans('Name') . ': ' . $fullName . "<br>" . $this->get('translator')->trans('Contacts') . ': ' . $contacts . "<br>" . $this->get('translator')->trans('Message') . ': ' . $text . "<br>" . $this->get('translator')->trans('Message from real estate page') . ': ' . $_SERVER['HTTP_REFERER'];

        if($mailService->send($this->get('translator')->trans('Review') . ': ' . DateHelper::getDateToMail(), 'noreaply@example.com', getenv('ADMIN_EMAIL'), $message)) {
            $messageService->increase();
            return new JsonResponse([
                'message' => $this->get('translator')->trans('Message was saved')
            ], 201);
        }

        return new JsonResponse([
            'type' => $this->get('translator')->trans('Some error, try again'),
            'error' => $message
        ], 400);
    }
    public function callbackAction(Request $request, MailService $mailService, MessageService $messageService)
    {
        if($messageService->canNotWrite()) {
            return new JsonResponse([
                'error' => $this->get('translator')->trans('To much requests. Max once in three minutes')
            ], 400);
        }

        $fullName = $request->request->get('fullname', '');
        $contacts = $request->request->get('contacts', '') . ' ' . $request->request->get('phone', '');

        $text = $request->request->get('text', '');

        $realty = $request->get('id') ? $this->get('translator')->trans('Object id')  . ': ' . $request->get('id') : '';

        $message = $this->get('translator')->trans('Name') . ': '. $fullName . "<br>". $this->get('translator')->trans('Contacts') . ': ' . $contacts . "<br>" . $this->get('translator')->trans('Message') . ': ' . $text . "<br>" . $this->get('translator')->trans('Message from real estate page') . ': ' . $_SERVER['HTTP_REFERER'];

        if($mailService->send($this->get('translator')->trans('Callback') . ': ' . DateHelper::getDateToMail(), 'noreaply@example.com', getenv('ADMIN_EMAIL'), $message)) {
            $messageService->increase();
            return new JsonResponse([
                'message' => $this->get('translator')->trans('Message was saved')
            ], 201);
        }


        return new JsonResponse([
            'type' => $this->get('translator')->trans('Some error, try again'),
            'error' => $message
        ], 400);
    }
    public function ownerAction(Request $request, MailService $mailService)
    {
        $dealType = $request->request->get('deal_type', $this->get('translator')->trans('Not specified'));
        $type = $request->request->get('type', $this->get('translator')->trans('Not specified'));

        $price = $request->request->get('price', $this->get('translator')->trans('Not specified'));
        $square = $request->request->get('square', $this->get('translator')->trans('Not specified'));
        $district = $request->request->get('district', $this->get('translator')->trans('Not specified'));
        $phone = $request->request->get('phone', $this->get('translator')->trans('Not specified'));

        $message = $this->get('translator')->trans('Deal type') . ': ' . $dealType . "<br>" . $this->get('translator')->trans('Realty type') . ': ' . $type . "<br>" . $this->get('translator')->trans('Price') . ': ' . $price . "<br>" . $this->get('translator')->trans('Square') . ': ' . $square . "<br>" . $this->get('translator')->trans('District name') . ": " . $district . "<br>". $this->get('translator')->trans('Phone') . ': ' . $phone;

        if($mailService->send($this->get('translator')->trans('From owners') . ': ' . DateHelper::getDateToMail(), 'noreaply@example.com', getenv('ADMIN_EMAIL'), $message)) {
            return new JsonResponse([
                'message' => $this->get('translator')->trans('Message was saved')
            ], 201);
        }

        return new JsonResponse([
            'type' => $this->get('translator')->trans('Some error, try again'),
            'error' => $message
        ], 400);
    }

    public function joinToTeamAction(Request $request, MailService $mailService)
    {
        $name = $request->get('name');
        $phone = $request->get('phone');

        $body = 'Name: ' . $name . '. Phone: ' . $phone;

        $isSend = $mailService->send($this->get('translator')->trans('Join the team') . ': ' . DateHelper::getDateToMail(), 'noreaply@example.com', getenv('ADMIN_EMAIL'), $body);

        if($isSend) {
            return new JsonResponse([
                'message' => $this->get('translator')->trans('Message was saved')
            ], 201);
        }

        return new JsonResponse([
            'message' => $this->get('translator')->trans('Some error')
        ], 400);
    }
}
