<?php

// src/Metrag/ApiBundle/Controller/ComplaintController.php
namespace App\Metrag\ApiBundle\Controller;

use App\Metrag\ApiBundle\Services\RealtyService;
use App\Metrag\ApiBundle\Transformers\DefaultTransformer;
use App\Metrag\ApiBundle\Transformers\RealtyStreetTransformer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class StreetController extends Controller
{
    public function getAllAction(Request $request, RealtyService $realtyService)
    {
        $realties = $realtyService->getRealtyOnFilter($request->query->all())->execute();

        return new JsonResponse((new RealtyStreetTransformer())->transform($realties));

    }
}
