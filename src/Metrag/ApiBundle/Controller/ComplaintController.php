<?php

// src/Metrag/ApiBundle/Controller/ComplaintController.php
namespace App\Metrag\ApiBundle\Controller;

use App\Metrag\ApiBundle\Services\ComplaintService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ComplaintController extends Controller
{
    public function addRealtyAction(Request $request, ComplaintService $complaintService)
    {
        $fullName = $request->request->get('fullname', '');
        $contacts = $request->request->get('contacts', '');
        $text = $request->request->get('text', '');

        try {
            $complaintService->store($fullName, $contacts, $text, $_SERVER['HTTP_REFERER'], ComplaintService::COMPLAINT_TYPES['TYPE_REALTY']);

            return new JsonResponse([
                'message' => $this->get('translator')->trans('Message was saved')
            ], 201);

        } catch(\Exception $exception) {
            $message = $exception->getMessage();
            return new JsonResponse([
                'type' => $this->get('translator')->trans('Some error, try again'),
                'error' => $message
            ], 400);
        }
    }
}
