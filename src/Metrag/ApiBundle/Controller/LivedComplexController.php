<?php

// src/Metrag/ApiBundle/LivedComplexController.php

namespace App\Metrag\ApiBundle\Controller;

use App\Metrag\ApiBundle\Transformers\LivedComplexListTransformer;
use App\Metrag\AppBundle\Services\LivedComplexService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class LivedComplexController extends Controller
{
    public function getListForInputAction(LivedComplexService $livedComplexService)
    {
        return new JsonResponse([
            'lived_complexes' => (new LivedComplexListTransformer())->transform($livedComplexService->getComplexes())
        ], 201);
    }
}
