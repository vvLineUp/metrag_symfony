<?php

// src/Metrag/ApiBundle/LivedComplexController.php

namespace App\Metrag\ApiBundle\Controller;

use App\Metrag\ApiBundle\Transformers\LivedComplexListTransformer;
use App\Metrag\ApiBundle\Transformers\LivedComplexTransformer;
use App\Metrag\AppBundle\Helpers\PaginationHelper;
use App\Metrag\AppBundle\Services\LivedComplexService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class LivedComplexStaticController extends Controller
{
    public function getAction(Request $request)
    {
        $livedComplexesLimit = getenv('LIVED_COMPLEXES_LIMIT');

        $page =  $request->query->getInt('page', 1);

        $livedComplexesCount = $livedComplexes = $this->getDoctrine()
            ->getRepository('AppBundle:LivedComplexStatic')
            ->count([]);

        $livedComplexes = $this->getDoctrine()
            ->getRepository('AppBundle:LivedComplexStatic')
            ->findBy([], ['id' => 'DESC'], 6, PaginationHelper::getOffset($page, 6));

        return new JsonResponse([
            'lived_complexes' => (new LivedComplexTransformer())->transform($livedComplexes),
            'count_pages' => PaginationHelper::getCounPages($livedComplexesCount, $livedComplexesLimit),
        ], 201);
    }
}
