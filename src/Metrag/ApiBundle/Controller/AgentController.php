<?php

// src/Metrag/ApiBundle/AgentController.php

namespace App\Metrag\ApiBundle\Controller;

use App\Metrag\ApiBundle\Transformers\AgentTransformer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class AgentController extends Controller
{
    public function getAction()
    {
        $agents = $this->getDoctrine()
            ->getRepository('AppBundle:Agent')
            ->findAll();

        return new JsonResponse([
            'agents' => (new AgentTransformer())->transform($agents)
        ], 201);
    }
}
