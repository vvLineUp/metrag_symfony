<?php


namespace App\Metrag\XmlBundle\Controller;

use App\Metrag\ApiBundle\Services\RealtyService;
use App\Metrag\XmlBundle\Services\XmlFeedGeneratorService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


class FeedController extends Controller
{
//    for test
    public function generateAction(XmlFeedGeneratorService $feedGeneratorService, RealtyService $realtyService): Response
    {
        $start = microtime(true);
        $realties =  $realtyService->getRealtyOnFilter([])->execute();

        $feedGeneratorService->generateFile($realties);
        $finish = microtime(true);

        $delta = $finish - $start;
        return new JsonResponse(['count' => count($realties),'time' => $delta],200);
        //return new Response($feedGeneratorService->generateFile($realties),200,['Content-type' => 'text/xml']);
    }

}
