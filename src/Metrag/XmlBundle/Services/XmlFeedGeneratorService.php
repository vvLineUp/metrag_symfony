<?php


namespace App\Metrag\XmlBundle\Services;


use App\Metrag\AgentBundle\Services\AgentService;
use App\Metrag\AppBundle\Entity\Realty;
use Carbon\Carbon;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class XmlFeedGeneratorService
{
    /**
     * @var array
     */
    private $mainParams;

    private $serializer;
    /**
     * @var UrlGeneratorInterface
     */
    private $router;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var AgentService
     */
    private $agentService;

    public function __construct(UrlGeneratorInterface $router,LoggerInterface $logger, AgentService $agentService)
    {
        $this->serializer = new Serializer([new ObjectNormalizer()], [new XmlEncoder('page')]);

        $this->mainParams = [
            '@xmlns' => 'http://www.w3.org/2001/XMLSchema-instance',
            '#' => [
                'generation_time' => Carbon::now()->toIso8601String(),
                'announcements' => [
                    'announcement' => []
                ]
            ]
        ];

        $this->router = $router;
        $this->logger = $logger;
        $this->agentService = $agentService;
    }

    public function generate(array $realties): string
    {

        foreach (array_splice($realties,0,2) as $realty) {
            $this->mainParams['#']['announcements']['announcement'][] = $this->buildItem($realty);
        }
        return $this->serializer->encode($this->mainParams, 'xml', [
            'xml_version' => '1.0',
            'xml_encoding' => 'utf-8',
            'xml_root_node_name' => 'page'
        ]);
    }

    public function generateFile(array $realties): bool
    {
        foreach ($realties as $realty) {
            $this->mainParams['#']['announcements']['announcement'][] = $this->buildItem($realty);
        }

        $fileSystem = new Filesystem();

        $xmlContent = $this->serializer->encode($this->mainParams, 'xml', [
            'xml_version' => '1.0',
            'xml_encoding' => 'utf-8',
            'xml_root_node_name' => 'page'
        ]);

        try {
            $new_file_path = '/var/www/public/feed.xml';

            $fileSystem->dumpFile($new_file_path, $xmlContent);

        } catch (IOExceptionInterface $exception) {
            echo "Error creating file at ". $exception->getPath();
            echo $exception->getMessage();
        }

        return true;
    }

    private function buildItem(Realty $realty): array
    {
        $agents = $this->agentService->getAgentOfRealty($realty);
        $firstAgent = $agents[0];


        $realtyXml = [
            'update_time' => Carbon::parse($realty->getDatetime())->toIso8601String(),
            'contract_type' => $realty->getDealType()->getName(),
            'realty_type' => $realty->getType()->getName(),
            'region' => 'Харьковская область',
            'city' => 'Харьков',
            'street' => $realty->getStreet(),
            'house' => $realty->getNumberBulding(),
            'room_count' => $realty->getRooms(),
            'floor' => $realty->getFloor(),
            'floor_count' => $realty->getFloorAll(),
            'total_area' => $realty->getSquare(),
            'living_area' => $realty->getSquareLiving(),
            'kitchen_area' => $realty->getSquareKitchen(),
            'price' => $realty->getPrice(),
            'currency' => '$',
            'built_year' => $realty->getBuiltYear(),
            'wc_type' => $realty->getIsCombinedBathroom(),
            'has_balcony' => $realty->getCountBalcony() > 0,
            'text' => $realty->getDescription(),
            'phones' => implode(',', $firstAgent->getNumbers()),
            'contact_name' => $firstAgent->getFullname(),
            'url' => getenv('APP_HOST').$this->router->generate('realty_show', ['id' => $realty->getId()]),
            'images' => [
                'image' => []
            ]
        ];

        foreach ($realty->getImages() as $image) {
            $realtyXml['images']['image'][] = $image;
        }

        if ($realty->getLivedComplex()) {
            $realtyXml['building'] = $realty->getLivedComplexName();
        }

        if($realty->getDistrict() !== null){
            $realtyXml['building'] = $realty->getDistrict()->getName();
        }

        return $realtyXml;
    }
}
